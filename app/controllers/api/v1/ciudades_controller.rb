module Api
    module V1
        class CiudadesController < ApplicationController
            before_action :set_ciudad, only: [:show, :destroy]
            
            def index
               @ciudades = Ciudad.todos_los_que_no_fueron_borrados_logicamente
            end
        
            def show    
            end
            
            def new
        
            end
        
            def create
                @ciudad = Ciudad.new(ciudad_params)
                @ciudad.save
            end
        
            def edit
            end
        
            def update 
                @ciudad = Ciudad.find(params[:id])
                @ciudad.update(ciudad_params)
            end
        
            def destroy
                @ciudad = Ciudad.find(params[:id])
                @ciudad.soft_destroy
            end
        
            private
        
            def ciudad_params
                params.require(:ciudad).permit(:denominacion, :cantidad_habitantes)
            end
        
            def set_ciudad
                @ciudad = Ciudad.find(params[:id])
            end
        end        
    end
end

