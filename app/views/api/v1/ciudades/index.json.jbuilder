json.ciudades @ciudades, :id, :denominacion do |ciudad|
    json.id ciudad.id
    json.denominacion ciudad.denominacion
    json.iconos ciudad.iconos_geograficos
end