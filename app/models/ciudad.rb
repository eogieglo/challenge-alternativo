# == Schema Information
#
# Table name: ciudades
#
#  id                  :integer          not null, primary key
#  cantidad_habitantes :integer
#  deleted_at          :datetime
#  denominacion        :string
#  superficie          :float
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
class Ciudad < ApplicationRecord
    has_many :iconos_geograficos

    scope :todos_los_que_no_fueron_borrados_logicamente, ->{
        where(deleted_at: nil)
    }

    def soft_destroy
        update(deleted_at: Time.now)
    end
end
